/*
 * Copyright (c) 2017 Oleg Oshmyan <chortos@inbox.lv>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
 * THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT,
 * OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
 * FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <limits>
#include <memory>
#include <queue>
#include <type_traits>
#include <utility>

#include <message.h>

namespace
{
	namespace dcj
	{
		static_assert(std::numeric_limits<int>::min() <= INT32_MIN &&
		              std::numeric_limits<int>::max() >= INT32_MAX,
		              "all values of type int32_t must be "
		              "representable in type int");
		static_assert(std::numeric_limits<long long>::min() <= INT64_MIN &&
		              std::numeric_limits<long long>::max() >= INT64_MAX,
		              "all values of type int64_t must be "
		              "representable in type long long");

		const int number_of_nodes = NumberOfNodes();
		const int my_node_id = MyNodeId();

		class node
		{
			int id;

			static char to_char(std::uint8_t value) noexcept
			{
				if (!std::numeric_limits<char>::is_signed)
					return value;

				std::int8_t min = INT8_MIN;
				std::uint8_t sign_bit = min;
				if (!(value & sign_bit))
					return value;

				return static_cast<std::int8_t>(value & ~sign_bit) | min;
			}

			static int to_int(std::uint32_t value) noexcept
			{
				std::int32_t min = INT32_MIN;
				std::uint32_t sign_bit = min;
				if (!(value & sign_bit))
					return value;

				return static_cast<std::int32_t>(value & ~sign_bit) | min;
			}

			static long long to_long_long(std::uint64_t value) noexcept
			{
				std::int64_t min = INT64_MIN;
				std::uint64_t sign_bit = min;
				if (!(value & sign_bit))
					return value;

				return static_cast<std::int64_t>(value & ~sign_bit) | min;
			}

		public:
			node(int id) noexcept: id(id) {}

			void put8(std::uint8_t value) { PutChar(id, to_char(value)); }
			void put32(std::uint32_t value) { PutInt(id, to_int(value)); }
			void put64(std::uint64_t value)
				{ PutLL(id, to_long_long(value)); }
			void send() { Send(id); }

			void receive() { Receive(id); }
			std::int8_t get8() { return GetChar(id); }
			std::uint8_t get8u() { return GetChar(id); }
			std::int32_t get32() { return GetInt(id); }
			std::uint32_t get32u() { return GetInt(id); }
			std::int64_t get64() { return GetLL(id); }
			std::uint64_t get64u() { return GetLL(id); }
		};

		template <typename T>
		class _circular_queue_base
		{
		protected:
			T *_data;
			std::size_t _capacity;

			_circular_queue_base(std::size_t capacity):
				_data(std::allocator<T>().allocate(_capacity)),
				_capacity(capacity)
			{}

			_circular_queue_base(const _circular_queue_base &) = delete;

			_circular_queue_base(_circular_queue_base &&other) noexcept:
				_data(other._data), _capacity(other._capacity)
			{
				other._data = nullptr;
				other._capacity = 0;
			}

			~_circular_queue_base()
			{ std::allocator<T>().deallocate(_data, _capacity); }
		};

		template <typename T>
		class circular_queue: _circular_queue_base<T>
		{
			class _const_iterator
			{
				const circular_queue *_queue;
				std::size_t _index;

				friend circular_queue;

				_const_iterator(const circular_queue &queue,
				                std::size_t index) noexcept:
					_queue(&queue), _index(index)
				{}

			public:
				typedef std::ptrdiff_t difference_type;
				typedef              T      value_type;
				typedef        const T *       pointer;
				typedef        const T &     reference;
				typedef std::random_access_iterator_tag iterator_category;

				_const_iterator() noexcept: _index() {}

				_const_iterator &operator++() noexcept
				{ ++_index; return *this; }

				_const_iterator operator++(int) noexcept
				{ _const_iterator p = *this; ++p; return p; }

				_const_iterator &operator--() noexcept
				{ --_index; return *this; }

				_const_iterator operator--(int) noexcept
				{ _const_iterator p = *this; --p; return p; }

				_const_iterator &operator+=(difference_type n) noexcept
				{ _index += n; return *this; }

				_const_iterator &operator-=(difference_type n) noexcept
				{ return *this += -n; }

				_const_iterator operator+(difference_type n) const noexcept
				{ _const_iterator p = *this; p += n; return p; }

				_const_iterator operator-(difference_type n) const noexcept
				{ _const_iterator p = *this; p -= n; return p; }

				difference_type operator-(_const_iterator other) const noexcept
				{ return _index - other._index; }

				reference operator*() const noexcept
				{
					if (_index >= _queue->_capacity)
						return _queue->_data[_index - _queue->_capacity];
					else
						return _queue->_data[_index];
				}

				reference operator[](difference_type n) const noexcept
				{ return *(*this + n); }

				pointer operator->() const noexcept { return &*this; }

				bool operator==(_const_iterator other) const noexcept
				{ return _index == other._index; }

				bool operator!=(_const_iterator other) const noexcept
				{ return _index != other._index; }

				bool operator<(_const_iterator other) const noexcept
				{ return _index < other._index; }

				bool operator<=(_const_iterator other) const noexcept
				{ return _index <= other._index; }

				bool operator>(_const_iterator other) const noexcept
				{ return _index > other._index; }

				bool operator>=(_const_iterator other) const noexcept
				{ return _index >= other._index; }
			};

			std::size_t _begin, _size;

			T *data() noexcept { return this->_data; }
			const T *data() const noexcept { return this->_data; }

			_const_iterator _cbegin() const noexcept
				{ return {*this, _begin}; }
			_const_iterator _cend() const noexcept
				{ return {*this, _begin + _size}; }

		public:
			typedef                T       value_type;
			typedef std::allocator<T>  allocator_type;
			typedef      std::size_t        size_type;
			typedef                T &      reference;
			typedef          const T &const_reference;

			explicit circular_queue(std::size_t _capacity):
				_circular_queue_base<T>(_capacity), _begin(0), _size(0)
			{}

			circular_queue(const circular_queue &other):
				_circular_queue_base<T>(other._capacity),
				_begin(0), _size(other.size())
			{
				if (other._size <= other._capacity - other._begin)
					std::uninitialized_copy_n(other._data + other._begin,
					                          other._size, data());
				else
				{
					std::uninitialized_copy(other._data + other._begin,
					                        other._data + other._capacity,
					                        data());
					auto offset = other._capacity - other._begin;
					auto remainder = other._size - offset;
					__try
					{
						std::uninitialized_copy(other._data,
						                        other._data + remainder,
						                        data() + offset);
					}
					__catch (...)
					{
						std::_Destroy(data(), data() + offset);
						__throw_exception_again;
					}
				}
			}

			circular_queue(circular_queue &&other) noexcept:
				_circular_queue_base<T>(std::move(other)),
				_begin(other._begin), _size(other._size)
			{ other._begin = other._size = 0; }

			~circular_queue()
			{
				if (_size <= capacity() - _begin)
					std::_Destroy(data() + _begin, data() + _begin + _size);
				else
				{
					auto remainder = _size - (capacity() - _begin);
					std::_Destroy(data() + _begin, data() + capacity());
					std::_Destroy(data(), data() + remainder);
				}
			}

			circular_queue &operator=(const circular_queue &other)
			{ return *this = circular_queue(other); }

			circular_queue &operator=(circular_queue &&other) noexcept
			{
				this->_data = other._data;
				this->_capacity = other._capacity;
				_begin = other._begin;
				_size = other._size;
				other._data = nullptr;
				other._capacity = other._begin = other._size = 0;
				return *this;
			}

			reference front() noexcept { return data()[_begin]; }
			const_reference front() const noexcept { return data()[_begin]; }

			reference back() noexcept
			{
				auto end = _begin + _size;
				if (end > capacity())
					end -= capacity();
				return data()[end - 1];
			}
			const_reference back() const noexcept
			{
				auto end = _begin + _size;
				if (end > capacity())
					end -= capacity();
				return data()[end - 1];
			}

			bool empty() const noexcept { return !_size; }
			bool full() const noexcept { return _size == capacity(); }
			size_type size() const noexcept { return _size; }
			size_type capacity() const noexcept { return this->_capacity; }

			void push(const value_type &value)
			noexcept(std::is_nothrow_copy_constructible<T>::value)
				{ emplace(value); }
			void push(value_type &&value)
			noexcept(std::is_nothrow_move_constructible<T>::value)
				{ emplace(std::move(value)); }

			template <typename... Args>
			void emplace(Args &&... args)
			noexcept(std::is_nothrow_constructible<T, Args...>::value)
			{
				auto end = _begin + _size;
				if (end >= capacity())
					end -= capacity();
				std::_Construct(data() + end, std::forward<Args>(args)...);
				++_size;
			}

			void pop() noexcept
			{
				std::_Destroy(data() + _begin++);
				if (_begin == capacity())
					_begin = 0;
				--_size;
			}

			void swap(circular_queue &other) noexcept
			{
				std::swap(this->_data, other._data);
				std::swap(this->_capacity, other._capacity);
				std::swap(_begin, other._begin);
				std::swap(_size, other._size);
			}

			friend void swap(circular_queue &lhs,
			                 circular_queue &rhs) noexcept
			{ lhs.swap(rhs); }

			friend bool operator==(const circular_queue &lhs,
			                       const circular_queue &rhs)
			{
				return lhs._size == rhs._size &&
				       std::equal(lhs._cbegin(), lhs._cend(), rhs._cbegin());
			}

			friend bool operator!=(const circular_queue &lhs,
			                       const circular_queue &rhs)
			{ return !(lhs == rhs); }

			friend bool operator<(const circular_queue &lhs,
			                      const circular_queue &rhs)
			{
				return std::lexicographical_compare(
					lhs._cbegin(), lhs._cend(), rhs._cbegin(), rhs._cend());
			}

			friend bool operator<=(const circular_queue &lhs,
			                       const circular_queue &rhs)
			{ return !(rhs < lhs); }

			friend bool operator>(const circular_queue &lhs,
			                      const circular_queue &rhs)
			{ return rhs < lhs; }

			friend bool operator>=(const circular_queue &lhs,
			                       const circular_queue &rhs)
			{ return !(lhs < rhs); }
		};

		template <typename T>
		constexpr bool _is_nothrow_swappable(std::true_type) noexcept
		{
			using std::swap;
			return noexcept(swap(std::declval<T &>(), std::declval<T &>()));
		}

		template <typename T>
		constexpr bool _is_nothrow_swappable(std::false_type) noexcept
		{ return false; }

		template <typename T>
		constexpr bool _is_nothrow_swappable() noexcept
		{
			typedef typename std::add_lvalue_reference<T>::type reference;
			return _is_nothrow_swappable<T>(std::is_reference<reference>());
		}

		constexpr struct nullopt_t
		{
			enum struct _enum { _value };
			explicit constexpr nullopt_t(_enum) noexcept {}
		} nullopt(nullopt_t::_enum::_value);

		constexpr struct in_place_t
		{
			explicit in_place_t() = default;
		} in_place{};

		template <typename T,
			bool TriviallyDestructible =
				std::is_trivially_destructible<T>::value>
		struct _optional_base {
			union { T _value; struct {} _nothing; };
			bool _full;

			constexpr _optional_base() noexcept: _nothing(), _full(false) {}

			_optional_base(const _optional_base &) = default;
			_optional_base(_optional_base &&) = default;
			_optional_base &operator=(const _optional_base &other) = delete;

			template <typename... Args>
			constexpr _optional_base(in_place_t, Args &&... args):
				_value(std::forward<Args>(args)...), _full(true) {}
		};

		template <typename T>
		struct _optional_base<T, false>
		{
			union { T _value; struct {} _nothing; };
			bool _full;

			constexpr _optional_base() noexcept: _nothing(), _full(false) {}

			_optional_base(const _optional_base &) = default;
			_optional_base(_optional_base &&) = default;
			_optional_base &operator=(const _optional_base &other) = delete;

			template <typename... Args>
			constexpr _optional_base(in_place_t, Args &&... args):
				_value(std::forward<Args>(args)...), _full(true) {}

			~_optional_base()
			{
				if (_full)
					std::_Destroy(std::addressof(_value));
			}
		};

		template <typename T,
			bool CopyConstructible = std::is_copy_constructible<T>::value,
			bool TriviallyCopyConstructible =
				std::is_trivially_copy_constructible<T>::value,
			bool MoveConstructible = std::is_move_constructible<T>::value,
			bool TriviallyMoveConstructible =
				std::is_trivially_move_constructible<T>::value>
		struct _optional_construct: _optional_base<T>
		{
			_optional_construct() = default;

			template <typename... Args>
			constexpr _optional_construct(in_place_t, Args &&... args):
				_optional_base<T>(in_place, std::forward<Args>(args)...) {}
		};

		template <typename T, bool B>
		struct _optional_construct<T, B, B, false, false>: _optional_base<T>
		{
			_optional_construct() = default;
			_optional_construct(_optional_construct &&other) = delete;

			template <typename... Args>
			constexpr _optional_construct(in_place_t, Args &&... args):
				_optional_base<T>(in_place, std::forward<Args>(args)...) {}
		};

		template <typename T, bool B>
		struct _optional_construct<T, B, B, true, false>: _optional_base<T>
		{
			_optional_construct() = default;

			_optional_construct(_optional_construct &&other)
			noexcept(std::is_nothrow_move_constructible<T>::value)
			{
				this->_full = other._full;
				if (this->_full)
					std::_Construct(std::addressof(this->_value),
					                std::move(other._value));
			}

			template <typename... Args>
			constexpr _optional_construct(in_place_t, Args &&... args):
				_optional_base<T>(in_place, std::forward<Args>(args)...) {}
		};

		template <typename T>
		struct _optional_construct<T, true, false, false, false>:
			_optional_base<T>
		{
			_optional_construct() = default;

			_optional_construct(const _optional_construct &other)
			noexcept(std::is_nothrow_copy_constructible<T>::value)
			{
				this->_full = other._full;
				if (this->_full)
					std::_Construct(std::addressof(this->_value),
					                other._value);
			}

			_optional_construct(_optional_construct &&) = delete;

			template <typename... Args>
			constexpr _optional_construct(in_place_t, Args &&... args):
				_optional_base<T>(in_place, std::forward<Args>(args)...) {}
		};

		template <typename T>
		struct _optional_construct<T, true, false, true, false>:
			_optional_base<T>
		{
			_optional_construct() = default;

			_optional_construct(const _optional_construct &other)
			noexcept(std::is_nothrow_copy_constructible<T>::value)
			{
				this->_full = other._full;
				if (this->_full)
					std::_Construct(std::addressof(this->_value),
					                other._value);
			}

			_optional_construct(_optional_construct &&other)
			noexcept(std::is_nothrow_move_constructible<T>::value)
			{
				this->_full = other._full;
				if (this->_full)
					std::_Construct(std::addressof(this->_value),
					                std::move(other._value));
			}

			template <typename... Args>
			constexpr _optional_construct(in_place_t, Args &&... args):
				_optional_base<T>(in_place, std::forward<Args>(args)...) {}
		};

		template <typename T>
		struct _optional_construct<T, true, false, true, true>:
			_optional_base<T>
		{
			_optional_construct() = default;

			_optional_construct(const _optional_construct &other)
			noexcept(std::is_nothrow_copy_constructible<T>::value)
			{
				this->_full = other._full;
				if (this->_full)
					std::_Construct(std::addressof(this->_value),
					                other._value);
			}

			_optional_construct(_optional_construct &&other) = default;

			template <typename... Args>
			constexpr _optional_construct(in_place_t, Args &&... args):
				_optional_base<T>(in_place, std::forward<Args>(args)...) {}
		};

		template <typename T>
		class optional: _optional_construct<T>
		{
			static_assert(
				!std::is_reference<T>::value &&
				!std::is_same<nullopt_t,
				              typename std::remove_cv<T>::type>::value,
				"optional cannot wrap reference types or nullopt_t");

		public:
			typedef T value_type;

			optional() = default;

			constexpr optional(nullopt_t) noexcept {}

			template <typename... Args,
				typename std::enable_if<
					std::is_constructible<T, Args &&...>::value
				>::type * = nullptr>
			constexpr explicit optional(in_place_t, Args &&... args)
			noexcept(std::is_nothrow_constructible<T, Args &&...>::value):
				_optional_construct<T>(in_place, std::forward<Args>(args)...)
			{}

			template <typename U, typename... Args,
				typename std::enable_if<
					std::is_constructible<T, std::initializer_list<U> &,
					                      Args &&...>::value
				>::type * = nullptr>
			constexpr explicit optional(in_place_t,
			                            std::initializer_list<U> ilist,
			                            Args &&... args)
			noexcept(std::is_nothrow_constructible<T, Args &&...>::value):
				_optional_construct<T>(in_place,
				                       ilist, std::forward<Args>(args)...)
			{}

			template <typename U = T,
				typename std::enable_if<
					std::is_constructible<T, U &&>::value &&
					!std::is_same<optional,
					              typename std::decay<U>::type>::value &&
					std::is_convertible<U &&, T>::value
				>::type * = nullptr>
			constexpr optional(U &&value)
			noexcept(std::is_nothrow_constructible<T, U &&>::value):
				_optional_construct<T>(in_place, std::forward<U>(value))
			{}

			template <typename U = T,
				typename std::enable_if<
					std::is_constructible<T, U &&>::value &&
					!std::is_same<optional,
					              typename std::decay<U>::type>::value &&
					!std::is_convertible<U &&, T>::value
				>::type * = nullptr>
			constexpr explicit optional(U &&value)
			noexcept(std::is_nothrow_constructible<T, U &&>::value):
				_optional_construct<T>(in_place, std::forward<U>(value))
			{}

			template <typename U,
				typename std::enable_if<
					std::is_constructible<T, const U &>::value &&
					!std::is_constructible<T, optional<U> &>::value &&
					!std::is_constructible<T, optional<U> &&>::value &&
					!std::is_constructible<T, const optional<U> &>::value &&
					!std::is_constructible<T, const optional<U> &&>::value &&
					!std::is_convertible<optional<U> &, T>::value &&
					!std::is_convertible<optional<U> &&, T>::value &&
					!std::is_convertible<const optional<U> &, T>::value &&
					!std::is_convertible<const optional<U> &&, T>::value &&
					std::is_convertible<const U &, T>::value
				>::type * = nullptr>
			optional(const optional<U> &other)
			noexcept(std::is_nothrow_constructible<T, const U &>::value)
			{
				this->_full = other._full;
				if (this->_full)
					std::_Construct(std::addressof(this->_value),
					                other._value);
			}

			template <typename U,
				typename std::enable_if<
					std::is_constructible<T, const U &>::value &&
					!std::is_constructible<T, optional<U> &>::value &&
					!std::is_constructible<T, optional<U> &&>::value &&
					!std::is_constructible<T, const optional<U> &>::value &&
					!std::is_constructible<T, const optional<U> &&>::value &&
					!std::is_convertible<optional<U> &, T>::value &&
					!std::is_convertible<optional<U> &&, T>::value &&
					!std::is_convertible<const optional<U> &, T>::value &&
					!std::is_convertible<const optional<U> &&, T>::value &&
					!std::is_convertible<const U &, T>::value
				>::type * = nullptr>
			explicit optional(const optional<U> &other)
			noexcept(std::is_nothrow_constructible<T, const U &>::value)
			{
				this->_full = other._full;
				if (this->_full)
					std::_Construct(std::addressof(this->_value),
					                other._value);
			}

			template <typename U,
				typename std::enable_if<
					std::is_constructible<T, U &&>::value &&
					!std::is_constructible<T, optional<U> &>::value &&
					!std::is_constructible<T, optional<U> &&>::value &&
					!std::is_constructible<T, const optional<U> &>::value &&
					!std::is_constructible<T, const optional<U> &&>::value &&
					!std::is_convertible<optional<U> &, T>::value &&
					!std::is_convertible<optional<U> &&, T>::value &&
					!std::is_convertible<const optional<U> &, T>::value &&
					!std::is_convertible<const optional<U> &&, T>::value &&
					std::is_convertible<U &&, T>::value
				>::type * = nullptr>
			optional(optional<U> &&other)
			noexcept(std::is_nothrow_constructible<T, U &&>::value)
			{
				this->_full = other._full;
				if (this->_full)
					std::_Construct(std::addressof(this->_value),
					                std::move(other._value));
			}

			template <typename U,
				typename std::enable_if<
					std::is_constructible<T, U &&>::value &&
					!std::is_constructible<T, optional<U> &>::value &&
					!std::is_constructible<T, optional<U> &&>::value &&
					!std::is_constructible<T, const optional<U> &>::value &&
					!std::is_constructible<T, const optional<U> &&>::value &&
					!std::is_convertible<optional<U> &, T>::value &&
					!std::is_convertible<optional<U> &&, T>::value &&
					!std::is_convertible<const optional<U> &, T>::value &&
					!std::is_convertible<const optional<U> &&, T>::value &&
					!std::is_convertible<U &&, T>::value
				>::type * = nullptr>
			explicit optional(optional<U> &&other)
			noexcept(std::is_nothrow_constructible<T, U &&>::value)
			{
				this->_full = other._full;
				if (this->_full)
					std::_Construct(std::addressof(this->_value),
					                std::move(other._value));
			}

			/*constexpr*/ T *operator->() noexcept
				{ return std::addressof(this->_value); }
			constexpr const T *operator->() const noexcept
				{ return std::addressof(this->_value); }

			/*constexpr*/ T &operator*() /*&*/ noexcept
				{ return this->_value; }
			constexpr const T &operator*() const /*&*/ noexcept
				{ return this->_value; }

			/*
			constexpr T &&operator*() && noexcept
				{ return std::move(this->_value); }
			constexpr const T &&operator*() const && noexcept
				{ return std::move(this->_value); }
			*/

			constexpr explicit operator bool() const noexcept
				{ return this->_full; }
			constexpr bool has_value() const noexcept { return this->_full; }

			template <typename U>
			constexpr T value_or(U &&value) const /*&*/
			noexcept(noexcept(static_cast<T>(std::forward<U>(value))))
			{
				return *this ? **this : static_cast<T>(std::forward<U>(value));
			}

			/*
			template <typename U>
			constexpr T value_or(U &&value) &&
			noexcept(noexcept(static_cast<T>(std::forward<U>(value))))
			{
				return *this ? std::move(**this)
				             : static_cast<T>(std::forward<U>(value));
			}
			*/

			friend constexpr bool
			operator==(const optional &lhs, const optional &rhs)
			{
				return lhs._full == rhs._full &&
				       (!lhs._full || lhs._value == rhs._value);
			}

			friend constexpr bool
			operator!=(const optional &lhs, const optional &rhs)
			{
				return lhs._full != rhs._full ||
				       lhs._full && lhs._value != rhs._value;
			}

			friend constexpr bool
			operator<(const optional &lhs, const optional &rhs)
			{ return rhs._full && (!lhs._full || lhs._value < rhs._value); }

			friend constexpr bool
			operator<=(const optional &lhs, const optional &rhs)
			{ return !lhs._full || rhs._full && lhs._value <= rhs._value; }

			friend constexpr bool
			operator>(const optional &lhs, const optional &rhs)
			{ return lhs._full && (!rhs._full || lhs._value > rhs._value); }

			friend constexpr bool
			operator>=(const optional &lhs, const optional &rhs)
			{ return !rhs._full || (lhs._full && lhs._value >= rhs._value); }

			friend constexpr bool
			operator==(const optional &self, nullopt_t) noexcept
				{ return !self._full; }
			friend constexpr bool
			operator==(nullopt_t, const optional &self) noexcept
				{ return !self._full; }

			friend constexpr bool
			operator!=(const optional &self, nullopt_t) noexcept
				{ return self._full; }
			friend constexpr bool
			operator!=(nullopt_t, const optional &self) noexcept
				{ return self._full; }

			friend constexpr bool
			operator<(const optional &self, nullopt_t) noexcept
				{ return false; }
			friend constexpr bool
			operator<=(const optional &self, nullopt_t) noexcept
				{ return !self._full; }
			friend constexpr bool
			operator>(const optional &self, nullopt_t) noexcept
				{ return self._full; }
			friend constexpr bool
			operator>=(const optional &self, nullopt_t) noexcept
				{ return true; }

			friend constexpr bool
			operator<(nullopt_t, const optional &self) noexcept
				{ return self._full; }
			friend constexpr bool
			operator<=(nullopt_t, const optional &self) noexcept
				{ return true; }
			friend constexpr bool
			operator>(nullopt_t, const optional &self) noexcept
				{ return false; }
			friend constexpr bool
			operator>=(nullopt_t, const optional &self) noexcept
				{ return !self.full; }

			friend constexpr bool
			operator==(const optional &self, const T &value)
				{ return self._full && self._value == value; }
			friend constexpr bool
			operator==(const T &value, const optional &self)
				{ return self._full && value == self._value; }

			friend constexpr bool
			operator!=(const optional &self, const T &value)
				{ return !self._full || self._value != value; }
			friend constexpr bool
			operator!=(const T &value, const optional &self)
				{ return !self._full || value != self._value; }

			friend constexpr bool
			operator<(const optional &self, const T &value)
				{ return !self._full || self._value < value; }
			friend constexpr bool
			operator<(const T &value, const optional &self)
				{ return self._full && value < self._value; }

			friend constexpr bool
			operator<=(const optional &self, const T &value)
				{ return !self._full || self._value <= value; }
			friend constexpr bool
			operator<=(const T &value, const optional &self)
				{ return self._full && value <= self._value; }

			friend constexpr bool
			operator>(const optional &self, const T &value)
				{ return self._full && self._value > value; }
			friend constexpr bool
			operator>(const T &value, const optional &self)
				{ return !self._full || value > self._value; }

			friend constexpr bool
			operator>=(const optional &self, const T &value)
				{ return self._full && self._value >= value; }
			friend constexpr bool
			operator>=(const T &value, const optional &self)
				{ return !self._full || value >= self._value; }
		};

		template <typename T>
		constexpr optional<typename std::decay<T>::type>
		make_optional(T &&value)
		noexcept(noexcept(
			optional<typename std::decay<T>::type>(std::forward<T>(value))))
		{
			typedef typename std::decay<T>::type decayed;
			return optional<decayed>(std::forward<T>(value));
		}

		template <typename T, typename... Args>
		constexpr optional<T> make_optional(Args &&... args)
		noexcept(noexcept(optional<T>(in_place, std::forward<Args>(args)...)))
		{ return optional<T>(in_place, std::forward<Args>(args)...); }

		template <typename T, typename U, typename... Args>
		constexpr optional<T> make_optional(std::initializer_list<U> ilist,
		                                    Args &&... args)
		noexcept(noexcept(optional<T>(in_place, ilist,
		                              std::forward<Args>(args)...)))
		{ return optional<T>(in_place, ilist, std::forward<Args>(args)...); }

		template <typename T,
			bool Enabled = std::is_copy_constructible<
				std::hash<typename std::remove_const<T>::type>
			>::value>
		class _optional_hash {};

		template <typename T>
		class _optional_hash<T, true>
		{
			typedef typename std::remove_const<T>::type _nonconst_type;

		public:
			typedef dcj::optional<T> argument_type;
			typedef size_t result_type;

			result_type operator()(const argument_type &arg) const
			noexcept(noexcept(std::hash<_nonconst_type>()(*arg)))
			{ return arg ? std::hash<_nonconst_type>()(*arg) : 0; }
		};

		template <typename Task>
		class node_pool
		{
			circular_queue<node> free_nodes;
			std::queue<Task> tasks;

		public:
			node_pool(): free_nodes(number_of_nodes - 1)
			{
				for (int i = 0; i < number_of_nodes; ++i)
					if (i != my_node_id)
						free_nodes.push(node(i));
			}

			~node_pool()
			{
				join();
				for (int i = 0; i < number_of_nodes; ++i)
				{
					if (i != my_node_id)
					{
						node node(i);
						Task::done(node);
						node.send();
					}
				}
			}

			void submit(const Task &task)
			{
				if (free_nodes.empty())
					tasks.push(task);
				else
				{
					node node = free_nodes.front();
					task.write(node);
					free_nodes.pop();
					node.send();
				}
			}

			void submit(Task &&task)
			{
				if (free_nodes.empty())
					tasks.push(std::move(task));
				else
				{
					node node = free_nodes.front();
					task.write(node);
					free_nodes.pop();
					node.send();
				}
			}

			void join()
			{
				while (!free_nodes.full())
				{
					node node(Receive(-1));
					// TODO: allow single message to contain multiple submits and a done
					optional<Task> &&task = Task::read(node);
					if (task)
						submit(std::move(*task));
					else if (tasks.empty())
						free_nodes.push(node);
					else
					{
						tasks.front().write(node);
						node.send();
						tasks.pop();
					}
				}
			}
		};

		template <typename Task>
		void worker_node(int master_node_id)
		{
			node master(master_node_id);
			for (;;)
			{
				master.receive();

				optional<Task> &&task = Task::read(master);
				if (!task)
					break;

				task->execute(master);

				Task::done(master);
				master.send();
			}
		}
	}
}

namespace std
{
	template <typename T>
	struct hash<dcj::optional<T>>: dcj::_optional_hash<T> {};
}

/*
struct task
{
	void execute(dcj::node master);
	void write(dcj::node node);
	static dcj::optional<task> read(dcj::node node);
	static void done(dcj::node node);
};
*/
